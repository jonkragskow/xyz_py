# xyz-py

`xyz-py` is a python module for working with and manipulating chemical structures. 

# Installation

For convenience, `xyz-py` is available on [PyPI](https://pypi.org/project/xyz-py/) and so can be installed using `pip`

```
pip install xyz-py
```

# Functionality

To use the functions included in `xyz-py`, first import it into your python file
```
import xyz_py as xyzp
```
and then call them as 
```
xyzp.function_name(arguments)
```

To find information on any of the functions included in `xyz-py`, use the `help` command from within a python environment, e.g.

```
help(xyzp.function_name)
```

or visit the [documentation](https://jonkragskow.gitlab.io/xyz_py/).
