# CHANGELOG


## v5.14.0 (2025-01-14)

### Continuous Integration

- Update config
  ([`a37e454`](https://gitlab.com/jonkragskow/xyz_py/-/commit/a37e45404214e527af2aead68cbc4f1cf749a5e9))

### Features

- Add cli inertia tensor calculator
  ([`8a969df`](https://gitlab.com/jonkragskow/xyz_py/-/commit/8a969df8c8abc52e060cc68511e3e69f728f14c6))


## v5.13.1 (2024-11-21)

### Performance Improvements

- Add ability to supply non_bond_label without index
  ([`7e0aa26`](https://gitlab.com/jonkragskow/xyz_py/-/commit/7e0aa26b800581293c780ec2593c67197a1ef00d))


## v5.13.0 (2024-09-06)

### Code Style

- Improve pep8 compliance
  ([`3fef1b0`](https://gitlab.com/jonkragskow/xyz_py/-/commit/3fef1b0a5823c013cd74306cd46e72c6d31cd82c))

### Continuous Integration

- Add rerun option
  ([`9913b2f`](https://gitlab.com/jonkragskow/xyz_py/-/commit/9913b2f8fa2be598844b217ab7917ce2a0970025))

### Features

- Add xyz format reader, support for missing headers, checks for missing headers, colourised
  terminal printing
  ([`99f38d0`](https://gitlab.com/jonkragskow/xyz_py/-/commit/99f38d0a230a0bd331eec0192860740d588fa8d1))


## v5.12.0 (2024-07-12)

### Features

- Add standardise_formdict method
  ([`8312f69`](https://gitlab.com/jonkragskow/xyz_py/-/commit/8312f698772cd84656823557861f5f0eea874b9c))

### Performance Improvements

- Improve struct_info cli
  ([`43d3556`](https://gitlab.com/jonkragskow/xyz_py/-/commit/43d3556398d53697dacc14b83738b2e29104e7c2))


## v5.11.2 (2024-06-28)

### Bug Fixes

- Add missing indices in bonds, angles and dihedrals output
  ([`6392980`](https://gitlab.com/jonkragskow/xyz_py/-/commit/63929803ef3a80a8a89c3de0317a992ab7109c2b))


## v5.11.1 (2024-06-19)

### Bug Fixes

- Fix error in loadxyz when atomic numbers specified
  ([`4c410ca`](https://gitlab.com/jonkragskow/xyz_py/-/commit/4c410caf1b3896b1ef45862716029ecec0543e6e))


## v5.11.0 (2024-04-22)

### Features

- Add configuration to check_xyz
  ([`4e58e0e`](https://gitlab.com/jonkragskow/xyz_py/-/commit/4e58e0e065389b1be93ad5a3eeeca38c0bf76f56))


## v5.10.0 (2024-04-18)

### Features

- Add ability to pass strings to remove_label_indices
  ([`17f7460`](https://gitlab.com/jonkragskow/xyz_py/-/commit/17f7460afd7cd9e9781d7775b99e18e7c6ef8a50))


## v5.9.4 (2024-04-04)

### Bug Fixes

- Add support for str in lab_to_num
  ([`2064b8a`](https://gitlab.com/jonkragskow/xyz_py/-/commit/2064b8aa5981d9ac902f8e3840f4779b5ea01bcc))

### Refactoring

- Add separate function for rotation matrix construction
  ([`0f40666`](https://gitlab.com/jonkragskow/xyz_py/-/commit/0f406661ebed36993c82ae32a5faff966c4c2d72))


## v5.9.3 (2024-02-22)

### Performance Improvements

- Increase tolerance for coordinate rotation, add typehints
  ([`0bcad5f`](https://gitlab.com/jonkragskow/xyz_py/-/commit/0bcad5f920d3d7f78465ba64b5baeacaaf3b9e6c))


## v5.9.2 (2024-02-22)

### Performance Improvements

- Increase tolerance for coordinate rotation, add typehints
  ([`2816971`](https://gitlab.com/jonkragskow/xyz_py/-/commit/281697124c5373119c70f2fe40fbf7d4276286bd))


## v5.9.1 (2024-01-24)

### Bug Fixes

- Manually bump docs vnum
  ([`ac63a02`](https://gitlab.com/jonkragskow/xyz_py/-/commit/ac63a02ba99068251b715382a6845b1426f00184))

### Build System

- Fix docs version number
  ([`b4014c0`](https://gitlab.com/jonkragskow/xyz_py/-/commit/b4014c0a4d4d7fe33377bc045cd5fe02ca2d2c65))

- Update .gitlab-ci.yml file
  ([`d228c21`](https://gitlab.com/jonkragskow/xyz_py/-/commit/d228c218a96c955241a68854bbd6c856bb8bd0bb))


## v5.9.0 (2024-01-24)

### Features

- Add option to specify non-bonding atms in find_entities and list_formulae
  ([`ae331a5`](https://gitlab.com/jonkragskow/xyz_py/-/commit/ae331a553486cfe51a8147d2ab563d1c6f9211c1))


## v5.8.0 (2023-11-10)

### Features

- Add denumber cli program
  ([`9f3df2b`](https://gitlab.com/jonkragskow/xyz_py/-/commit/9f3df2ba6e7b3ff7bfa98fa1d06fdda73afb0779))


## v5.7.1 (2023-09-21)

### Bug Fixes

- Remove label renumbering in struct_info cli
  ([`73d133f`](https://gitlab.com/jonkragskow/xyz_py/-/commit/73d133ffcbb4fb47432f164653c5d739ca7ea846))

### Continuous Integration

- Remove rename
  ([`4d8198d`](https://gitlab.com/jonkragskow/xyz_py/-/commit/4d8198d17004f422a6e640c400b277b553da424b))


## v5.7.0 (2023-07-26)

### Build System

- Update pysemver config
  ([`2185160`](https://gitlab.com/jonkragskow/xyz_py/-/commit/218516011826d9a807d553958fd7d8b4d2e9cf33))

### Continuous Integration

- Add checkout to before script
  ([`1e7e663`](https://gitlab.com/jonkragskow/xyz_py/-/commit/1e7e66387e07fdc2de821d7ce51cf8e9c8404b7c))

- Edit semrel config
  ([`11575aa`](https://gitlab.com/jonkragskow/xyz_py/-/commit/11575aacef4d862f15f77ebd14481427062803d5))

- Modify token
  ([`bc06cb3`](https://gitlab.com/jonkragskow/xyz_py/-/commit/bc06cb3283f9923d9826c4dd1137d25d9638fdf6))

- Remove ci token
  ([`b914463`](https://gitlab.com/jonkragskow/xyz_py/-/commit/b914463cfa992dd9301d9e95203a4cdf9b197688))

- Remove duplicate token def
  ([`f67a34b`](https://gitlab.com/jonkragskow/xyz_py/-/commit/f67a34b470c17891640bfde6ddedb696df8ea3be))

- Simplify semrel config
  ([`195a687`](https://gitlab.com/jonkragskow/xyz_py/-/commit/195a687ac424456304db90212f625ac33d2d2a8c))

- Tweak pysemver config
  ([`7e846aa`](https://gitlab.com/jonkragskow/xyz_py/-/commit/7e846aa659455ba22ede862b031897c86b87de19))

- Tweak token name
  ([`13ef945`](https://gitlab.com/jonkragskow/xyz_py/-/commit/13ef9452f0064ba6a5e7353969ee0ddd82a3d866))

- Update docs ci
  ([`d7f0b4e`](https://gitlab.com/jonkragskow/xyz_py/-/commit/d7f0b4efa84e79a8fb29e52e37fe80d582d3e2ae))

- Update pysemver config
  ([`48c1073`](https://gitlab.com/jonkragskow/xyz_py/-/commit/48c10735aff125bdb01af6aea46ca29aab59cc4e))
