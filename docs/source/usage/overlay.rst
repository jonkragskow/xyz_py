.. _overlay:

Overlay
=======

This subprogram combines two structures from different xyz file and attempts to overlay them. Note, this requires two structures of the same length.

To compute the 'best' overlay of the two structures, an element-wise RMSD is minimised assuming that the order of the two structures is the same.

Example
-------

The following ``.xyz`` file contains the structure of benzene.

.. code-block::
   :caption: ``benzene.xyz``

    12
    benzene
    H      1.2194     -0.1652      2.1600
    C      0.6825     -0.0924      1.2087
    C     -0.7075     -0.0352      1.1973
    H     -1.2644     -0.0630      2.1393
    C     -1.3898      0.0572     -0.0114
    H     -2.4836      0.1021     -0.0204
    C     -0.6824      0.0925     -1.2088
    H     -1.2194      0.1652     -2.1599
    C      0.7075      0.0352     -1.1973
    H      1.2641      0.0628     -2.1395
    C      1.3899     -0.0572      0.0114
    H      2.4836     -0.1022      0.0205

and the following ``.xyz`` file contains the structure of benzene which has been rotated by some unknown amount.

.. code-block::
   :caption: ``benzene_rotated.xyz`` Containing rotated benzene coordinates

    12
    benzene rotated
    H1         -1.7930941       1.6622742      -0.4488713
    C2         -1.0035120       0.9301677      -0.2511004
    C3          0.3138480       1.2187591      -0.5928308
    H4          0.5609968       2.1776562      -1.0594230
    C5          1.3171725       0.2885513      -0.3416732
    H6          2.3537977       0.5155788      -0.6106754
    C7          1.0034668      -0.9302425       0.2512500
    H8          1.7930655      -1.6621832       0.4488414
    C9         -0.3138480      -1.2187591       0.5928308
    H10        -0.5606985      -2.1778507       1.0592151
    C11        -1.3172662      -0.2885713       0.3417018
    H12        -2.3538463      -0.5155240       0.6105544

Running ``xyz_py overlay benzene.xyz benzene_rotated.xyz`` yields the two structures combined into a single ``.xyz`` file.

.. code-block::
   :caption: ``overlayed.xyz`` containing two overlayed benzene structures ith an rmsd of 0.0000 Angstrom.

    24

    H1          0.0000000       0.0000000       0.0000000
    C2          0.7895822      -0.7321064       0.1977709
    C3          2.1069421      -0.4435150      -0.1439594
    H4          2.3540909       0.5153821      -0.6105516
    C5          3.1102667      -1.3737227       0.1071982
    H6          4.1468919      -1.1466951      -0.1618039
    C7          2.7965611      -2.5925166       0.7001214
    H8          3.5861598      -3.3244571       0.8977128
    C9          1.4792463      -2.8810331       1.0417022
    H10         1.2323958      -3.8401247       1.5080865
    C11         0.4758280      -1.9508455       0.7905732
    H12        -0.5607521      -2.1777982       1.0594257
    H1          0.0000000       0.0000000       0.0000000
    C2          0.7895821      -0.7321065       0.1977709
    C3          2.1069421      -0.4435151      -0.1439595
    H4          2.3540909       0.5153820      -0.6105517
    C5          3.1102666      -1.3737229       0.1071981
    H6          4.1468918      -1.1466954      -0.1618041
    C7          2.7965609      -2.5925167       0.7001213
    H8          3.5861596      -3.3244574       0.8977127
    C9          1.4792461      -2.8810333       1.0417021
    H10         1.2323956      -3.8401249       1.5080864
    C11         0.4758279      -1.9508455       0.7905731
    H12        -0.5607522      -2.1777982       1.0594257
