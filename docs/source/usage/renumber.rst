.. _renumber:

Renumber
========

This subprogram renumbers the atom labels of a ``.xyz`` file to use either per-element (``Dy1, Dy2, H1, C1, H2, C2``) or sequential (``H1, C2, O3, C4, H5``) numbering. 

The input ``.xyz`` file can be provided with some, all, or no labelled atoms, but must conform to the standard xyz file layout.

Example
-------

The following ``.xyz`` file contains the structure of benzene without atom labels.

.. code-block::
   :caption: ``benzene.xyz``

    12
    benzene
    H      1.2194     -0.1652      2.1600
    C      0.6825     -0.0924      1.2087
    C     -0.7075     -0.0352      1.1973
    H     -1.2644     -0.0630      2.1393
    C     -1.3898      0.0572     -0.0114
    H     -2.4836      0.1021     -0.0204
    C     -0.6824      0.0925     -1.2088
    H     -1.2194      0.1652     -2.1599
    C      0.7075      0.0352     -1.1973
    H      1.2641      0.0628     -2.1395
    C      1.3899     -0.0572      0.0114
    H      2.4836     -0.1022      0.0205

Renumbering with ``xyz_py renumber benzene.xyz`` returns the same file with per-element labels.

.. code-block::
   :caption: ``benzene.xyz`` Renumbered in per-element style

    12
    benzene
    H1          1.2194000      -0.1652000       2.1600000 
    C1          0.6825000      -0.0924000       1.2087000 
    C2         -0.7075000      -0.0352000       1.1973000 
    H2         -1.2644000      -0.0630000       2.1393000 
    C3         -1.3898000       0.0572000      -0.0114000 
    H3         -2.4836000       0.1021000      -0.0204000 
    C4         -0.6824000       0.0925000      -1.2088000 
    H4         -1.2194000       0.1652000      -2.1599000 
    C5          0.7075000       0.0352000      -1.1973000 
    H5          1.2641000       0.0628000      -2.1395000 
    C6          1.3899000      -0.0572000       0.0114000 
    H6          2.4836000      -0.1022000       0.0205000 

Renumbering with ``xyz_py renumber benzene.xyz --style sequential`` returns the same file with sequential labels.

.. code-block::
   :caption: ``benzene.xyz`` Renumbered in sequential style

    12
    benzene
    H1          1.2194000      -0.1652000       2.1600000
    C2          0.6825000      -0.0924000       1.2087000
    C3         -0.7075000      -0.0352000       1.1973000
    H4         -1.2644000      -0.0630000       2.1393000
    C5         -1.3898000       0.0572000      -0.0114000
    H6         -2.4836000       0.1021000      -0.0204000
    C7         -0.6824000       0.0925000      -1.2088000
    H8         -1.2194000       0.1652000      -2.1599000
    C9          0.7075000       0.0352000      -1.1973000
    H10         1.2641000       0.0628000      -2.1395000
    C11         1.3899000      -0.0572000       0.0114000
    H12         2.4836000      -0.1022000       0.0205000
