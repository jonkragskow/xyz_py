.. _struct_info:

Structural Information
======================

This subprogram computes bond lengths, angles, and dihedrals for a given structure using `ASE <https://wiki.fysik.dtu.dk/ase/>`_.

Example
-------

The following ``.xyz`` file contains the structure of benzene.

.. code-block::
   :caption: ``benzene.xyz``

    12
    benzene
    H      1.2194     -0.1652      2.1600
    C      0.6825     -0.0924      1.2087
    C     -0.7075     -0.0352      1.1973
    H     -1.2644     -0.0630      2.1393
    C     -1.3898      0.0572     -0.0114
    H     -2.4836      0.1021     -0.0204
    C     -0.6824      0.0925     -1.2088
    H     -1.2194      0.1652     -2.1599
    C      0.7075      0.0352     -1.1973
    H      1.2641      0.0628     -2.1395
    C      1.3899     -0.0572      0.0114
    H      2.4836     -0.1022      0.0205

Running ``xyz_py struct_info benzene.xyz`` produces the following output

.. code-block:: bash

    12 bonds
    18 angles
    24 dihedrals
    Bonds, angles, and dihedrals written to benzene_<property>.csv

where the output file names are specified and contain the structural information.

These values are computed under the approximation that a bond is formed between two atoms if their distance falls
between the sum of their covalent radii, as defined in ASE. The radii are given in ``ase.data.covalent_radii`` and can be manually
changed using the ``--cutoffs ['symbol', 'cutoff'] [['symbol', 'cutoff'] ...]`` argument in ``xyz_py struct_info``.

For example, the default radius for hydrogen is 0.31 Angstrom, to change this to 0.5 use

.. code-block::

    xyz_py struct_info benzene.xyz --cutoffs H 0.5
