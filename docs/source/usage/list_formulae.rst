.. _list_formulae:

List Formulae
=============

This subprogram lists the formulae and number of entities present in a given ``.xyz`` file.

Example
-------

The following ``.xyz`` file contains the structure of two separate benzene and ammonia molecules.

.. code-block::
   :caption: ``benzene_ammonia.xyz``

    16
    benzene and ammonia
    H      1.2194     -0.1652      2.1600
    C      0.6825     -0.0924      1.2087
    C     -0.7075     -0.0352      1.1973
    H     -1.2644     -0.0630      2.1393
    C     -1.3898      0.0572     -0.0114
    H     -2.4836      0.1021     -0.0204
    C     -0.6824      0.0925     -1.2088
    H     -1.2194      0.1652     -2.1599
    C      0.7075      0.0352     -1.1973
    H      1.2641      0.0628     -2.1395
    C      1.3899     -0.0572      0.0114
    H      2.4836     -0.1022      0.0205
    N      4.9647     -0.0440      0.0285
    H      5.2658      0.6496      0.6822
    H      5.7774     -0.4532     -0.3850
    H      4.4478      0.4148     -0.6935

Running ``xyz_py list_formulae benzene_ammonia.xyz`` gives 

.. code-block::

    C6H6 : 1
    H3N : 1


The bonding is traced using the approximation that a bond is formed between two atoms if their distance falls
between the sum of their covalent radii, as defined in ASE. The radii are given in ``ase.data.covalent_radii`` and can be manually
changed using the ``--cutoffs ['symbol', 'cutoff'] [['symbol', 'cutoff'] ...]`` argument in ``xyz_py list_formulae``.