.. _rotate:

Rotate
======

This subprogram rotates a structure by a given set of values :math:`\alpha, \beta, \gamma` according to the `zyz convention <https://easyspin.org/easyspin/documentation/eulerangles.html>`_

Example
-------

The following ``.xyz`` file contains the structure of benzene.

.. code-block::
   :caption: ``benzene.xyz``

    12
    benzene
    H      1.2194     -0.1652      2.1600
    C      0.6825     -0.0924      1.2087
    C     -0.7075     -0.0352      1.1973
    H     -1.2644     -0.0630      2.1393
    C     -1.3898      0.0572     -0.0114
    H     -2.4836      0.1021     -0.0204
    C     -0.6824      0.0925     -1.2088
    H     -1.2194      0.1652     -2.1599
    C      0.7075      0.0352     -1.1973
    H      1.2641      0.0628     -2.1395
    C      1.3899     -0.0572      0.0114
    H      2.4836     -0.1022      0.0205

To rotate this by 90 degrees around :math:`z`, use ``xyz_py rotate benzene.xyz 90 0 0``.
