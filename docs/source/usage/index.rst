.. _guide:

Contents
========

.. toctree::
   :maxdepth: 1

    Structural Information <struct_info>
    Rotate <rotate>
    Overlay <overlay>
    List Formuale <list_formulae>
    Renumber <renumber>


Although ``xyz_py`` is intended to be imported into scripts and python programs, it also comes with a few command line interface programs which are explained within these pages.