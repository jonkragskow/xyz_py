Home
====

.. toctree::
   Installation <installation>
   Usage <usage/index>
   Modules <modules/index>
   Contributors <contribs>
   :maxdepth: 3
   :caption: Contents:
   :hidden:


``xyz_py`` is a small python module which carries out simple structure manipulations using `ASE <https://wiki.fysik.dtu.dk/ase/>`_.