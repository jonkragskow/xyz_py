Installation
============

The ``xyz_py`` package and its command line interface can be installed using the ``pip`` package manager

.. code-block:: bash

    pip install xyz_py

To test this was successful, run the following command

.. code-block:: bash

    xyz_py -h

You should see some help text and information about the program.